# General
variable "resource_group_name" {
  description = "The name of the resource group"
  default     = "rg-terraform"
}

variable "location" {
  description = "The Azure region in which all resources should be created"
  default     = "West Europe"
}

# App Service
variable "app_service_plan_name" {
  description = "The name of the app service plan for the backend"
  default     = "azapp-plan-terraform"
}

# Application OS
variable "app_service_plan_kind" {
  description = "The name of the application OS type for the backend"
  default     = "Windows"
}

variable "app_service_name" {
  description = "The name of the app service for the backend"
  default     = "azapp-terraform"
}

# Application Insights
variable "application_insights_name" {
  description = "The name of the application insights resource"
  default     = "appi-terraform"
}
