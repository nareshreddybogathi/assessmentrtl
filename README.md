# Cloud DevOps Engineer Assessment

##### 1) 

![picture](/img/RTL1.png)

- 
    - This architecture builds on the one shown in Basic a Webshop application. this design will resolve this below requirements:
        - Highly available.
        - Self-healing.
        - Highly scalable.
        - Has the ability to process large amounts of data.
        - Serve customers in Europe as well as USA. 
        - Should have secure measures “whenever possible”.
        - Should provide the best possible performance. It should allow for quick responses and quick load times for the web app client side.

    - Primary and secondary regions. This architecture uses two regions to achieve higher availability. The application is deployed to each region. Both regions are active/active, and requests are load balanced between them with trafic manager. If one region becomes unavailable, it is taken out of rotation.

    - This architecture includes the following components:

        - ##### Azure DNS: 
            Azure DNS is a hosting service for DNS domains, providing name resolution using Microsoft Azure infrastructure. By hosting your domains in Azure, you can manage your DNS records using the same credentials, APIs, tools, and billing as your other Azure services.
        
        - ##### Azure Traffic Manager:
            Azure Traffic Manager is a DNS-based load balancer to manage user traffic distribution of service endpoints in different data centers. This tool can service any of the Azure global regions and secure an optimal level of availability and responsiveness for your services for any Disaster recovery and Business continuity.

        - ##### Azure CDN:
            Azure Content Delivery Network (CDN) to cache publicly available content for lower latency and faster delivery of content.

        - ##### Active Directory:
            Azure AD is an Identity and Access Management (IAM) system. It provides a single place to store information about digital identities. you can use for authenticating app logins

        - ##### Azure load balancer:
            Regional load-balancing services distribute traffic within virtual networks across zonal and zone-redundant service endpoints within a region.
        
        - ##### AKS:
            Azure Kubernetes Service (AKS) offers serverless Kubernetes, an integrated continuous integration and continuous delivery (CI/CD) experience, and enterprise-grade security and governance. Unite your development and operations teams on a single platform to rapidly build, deliver, and scale applications with confidence.

        - ##### Database:
            Data storage. Use Azure SQL Database for relational data. 
            For non-relational data, consider Cosmos DB.
        
        - ##### Cache:
            We can improve performance and scalability by using Azure Cache for Redis to cache some data. Consider using Azure Cache for Redis for : Semi-static transaction data, Session state, HTML output. This can be useful in applications that render complex HTML output

        - ##### Geo-replication 
            Geo-replication need to enable for high availability, this also helps on disaster or regional failover still databse will work with secondary rgion.There is no manual action required for this failover.

###########################################################################################
 ##### 2)

- You’re new to an existing Azure Environment, provide the most important two security points that you would check for,
    - a.	VMs.
    - b.	App Services.
    - c.	Storage Accounts 
    - d.	Key Vaults
    - e.	RBAC
    - f.	AAD

        - VMs
            - Isolate management ports on virtual machines from the Internet and open them only when required.
            - Use complexity for passwords and user account names.
            - Keep the operating system patched.
            - Actively monitor for threats.
        - App Services
            - Service-to-service authentication.
            - Client authentication and authorization.
            - Check for Latest Version of services(java, .net, python...)
        - Storage Accounts
            - Allow Shared Access Signature Tokens Over HTTPS Only
            - Regenerate Storage Account Access Keys Periodically
        - Key Vaults
            - Check for Sufficient Certificate Auto-Renewal Period
            - Check for Certificate Minimum Key Size
            - Check for Azure Key Vault Secrets Expiration Date
        - RBAC
            - Limit the number of subscription owners
            - Assign roles to groups, not users
            - Use Azure AD Privileged Identity Management
        - AAD
            - Allow Only Administrators to Create Security Groups
            - Disable Remembering Multi-Factor Authentication.
            - Limit Guest User Permissions


##### 3)

- ### Prerequisites:
Below are prerequisites which should be there before running terraform locally

- Azure account with subscription, then you can login with
    ```console
    az login
    ```

- Then Check Terraform version
    ```console
    terraform -v
    ```
If terrraform is not exist in you machine, Try to install using this [link](https://learn.hashicorp.com/tutorials/terraform/install-cli?in=terraform/azure-get-started)


### Getting Started
#### Creating Infrastracture with Terraform

![picture](/img/terraform.png)

- Clone source code from gitlab repository
    ```console
    git clone https://gitlab.com/nareshreddybogathi/assessmentrtl.git
    cd assessmentrtl
    ```
- Login to your AZ account for creating Infrastracture.

    ```console
    az login
    ```
    You will be redirected to do your azure login and then to be able to work on the terminal. If everything is ok, you should see the following:
    ![picture](/img/az_login.png)

- Go into the project directory in source code and run `terraform init`. This will initialize the project, which downloads a plugin that allows Terraform to interact with our project.

    ```console
    terraform init
    ```

    ![picture](/img/terraform_init.png)

- Terraform Plans are usually run to `validate configuration changes` and confirm that the resulting actions are as expected.
    ```console
    terraform plan
    ```
    ![picture](/img/terraform_plan_1.png) 
    ![picture](/img/terraform_plan_2.png)

- Provision all resources which requireds for this project.

    ```console
    terraform apply
    ```
    When Terraform asks you to confirm type `yes` and press ENTER.
    ![picture](/img/terraform_apply_1.png) 


### Remove this demo resources 
If you want to remove the infrastracture follow the steps:

Go into the project root directory in source code and run `terraform destroy`. To confirm deletion please enter `yes`.
